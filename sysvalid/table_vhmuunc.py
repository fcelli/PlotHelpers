from prot import plottools
from prot import filetools
from prot import canvastools
from prot import utiltools

import ROOT

def calc_unc(ptcut):
    binmin=utiltools.Get('sig:/nominal/Zprime_pt').FindBin(ptcut)
    binmax=utiltools.Get('sig:/nominal/Zprime_pt').GetNbinsX()
    print(ptcut,utiltools.Get('sig:/nominal/Zprime_pt').GetBinLowEdge(binmin))

    xs_nom=utiltools.Get('sig:/nominal/Zprime_pt').Integral(binmin,binmax)

    xs_var=[]
    xs_var.append(utiltools.Get('sig:/muR05muF05/Zprime_pt').Integral(binmin,binmax))
    xs_var.append(utiltools.Get('sig:/muR05muF10/Zprime_pt').Integral(binmin,binmax))
    xs_var.append(utiltools.Get('sig:/muR10muF05/Zprime_pt').Integral(binmin,binmax))

    xs_var.append(utiltools.Get('sig:/muR10muF20/Zprime_pt').Integral(binmin,binmax))
    xs_var.append(utiltools.Get('sig:/muR20muF10/Zprime_pt').Integral(binmin,binmax))
    xs_var.append(utiltools.Get('sig:/muR20muF20/Zprime_pt').Integral(binmin,binmax))

    xs_var_diff=[(xs-xs_nom)/xs_nom for xs in xs_var]

    br=5.824E-01
    print('{:0.2f} +{:0.2f}% {:0.2f}%'.format(xs_nom*1e3/br,max(xs_var_diff)*100,min(xs_var_diff)*100))
    

def main():
    filetools.filemap('ggZH.root','sig')

    calc_unc(0)    
    calc_unc(400)
    calc_unc(430)
    calc_unc(450)
    calc_unc(500)
