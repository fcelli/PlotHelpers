from prot import plottools
from prot import filetools
from prot import canvastools

import ROOT

def main():
    filetools.filemap('ggZH.root','vh')
    title='ggZH'

    hs_pdfs=[('vh:/nominal',{'title':'NNPDF30_nlo_as_0118','color':ROOT.Blind1})]
    for i in range(1,101):
        hs_pdfs.append(('vh:/NNPDF30_nlo_as_0118_{:03d}'.format(i),{'title':'NNPDF30_nlo_as_0118' if i==0 else '','color':ROOT.Blind1}))

    for i in range(0,33):
        hs_pdfs.append(('vh:/PDF4LHC15_nlo_30_pdfas_{:03d}'.format(i),{'title':'PDF4LHC15_nlo_30_pdfas' if i==0 else '','color':ROOT.Blind2}))
        
    plottools.plotsf(hs_pdfs,
                     'fatjet0_m',
                     hsopt='nostack',opt='hist',
                     xtitle='leading jet mass [GeV]',xrange=(0,200),
                     ytitle='#sigma #times BR [pb]',
                     ratio=0,ratiorange=(0.9,1.1),
                     legend=(0.2,0.73),text='{}, H#rightarrowbb, p_{{T,H}}>500 GeV'.format(title),textpos=(0.2,0.8))
    canvastools.save('sysvalid/vh/pdf.pdf')

    plottools.plotsf(hs_pdfs,
                     'fatjet0_m',
                     hsopt='nostack',opt='hist',
                     normalize=(110,130),
                     xtitle='leading jet mass [GeV]',xrange=(0,200),
                     ytitle='#sigma #times BR [pb]',
                     ratio=0,ratiorange=(0.9,1.1),
                     legend=(0.2,0.73),text='{}, H#rightarrowbb, p_{{T,H}}>500 GeV'.format(title),textpos=(0.2,0.8))
    canvastools.save('sysvalid/vh/pdf-norm.pdf')
    
