from prot import plottools
from prot import filetools
from prot import canvastools

import ROOT

def main():
    filetools.filemap('VH.root','sig')
    

    plottools.plotsf([('sig:',{'title':'Nominal'}),
                      ('sig:/muR10muF05',{'title':'#mu_{R}#times1.0, #mu_{F}#times0.5','color':ROOT.Blind1}),
                      ('sig:/muR10muF20',{'title':'#mu_{R}#times1.0, #mu_{F}#times2.0','color':ROOT.Blind2})],
                     'fatjet0_m',
                     hsopt='nostack',opt='hist',
                     xtitle='leading jet mass [GeV]',xrange=(0,200),
                     ytitle='#sigma #times BR [pb]',
                     ratio=0,ratiorange=(0.9,1.1),
                     legend=(0.2,0.73),text='VH, H#rightarrowbb, p_{T,H}>500 GeV',textpos=(0.2,0.8))
    canvastools.save('sysvalid/vh/muF.pdf')

    plottools.plotsf([('sig:',{'title':'Nominal'}),
                      ('sig:/muR05muF10',{'title':'#mu_{R}#times0.5, #mu_{F}#times1.0','color':ROOT.Blind1}),
                      ('sig:/muR20muF10',{'title':'#mu_{R}#times2.0, #mu_{F}#times1.0','color':ROOT.Blind2})],
                     'fatjet0_m',
                     hsopt='nostack',opt='hist',
                     xtitle='leading jet mass [GeV]',xrange=(0,200),
                     ytitle='#sigma #times BR [pb]',
                     ratio=0,ratiorange=(0.9,1.1),
                     legend=(0.2,0.73),text='VH, H#rightarrowbb, p_{T,H}>500 GeV',textpos=(0.2,0.8))
    canvastools.save('sysvalid/vh/muR.pdf')    

    plottools.plotsf([('sig:',{'title':'Nominal'}),
                      ('sig:/muR05muF05',{'title':'#mu_{R}#times0.5, #mu_{F}#times0.5','color':ROOT.Blind1}),
                      ('sig:/muR20muF20',{'title':'#mu_{R}#times2.0, #mu_{F}#times2.0','color':ROOT.Blind2})],
                     'fatjet0_m',
                     hsopt='nostack',opt='hist',
                     xtitle='leading jet mass [GeV]',xrange=(0,200),
                     ytitle='#sigma #times BR [pb]',
                     ratio=0,ratiorange=(0.8,1.2),
                     legend=(0.2,0.73),text='VH, H#rightarrowbb, p_{T,H}>500 GeV',textpos=(0.2,0.8))
    canvastools.save('sysvalid/vh/muRmuF.pdf')    
    
