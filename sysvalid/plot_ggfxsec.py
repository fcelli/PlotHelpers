from prot import plottools
from prot import filetools
from prot import canvastools
from prot import utiltools

import ROOT

def make_xsecgraph(dirpath):
    h_pt=utiltools.Get(dirpath+'/Zprime_pt')

    g_xsec=ROOT.TGraph()
    gidx=0
    for binidx in range(1,h_pt.GetNbinsX()+1):
        pt=h_pt.GetBinLowEdge(binidx)
        xsec=h_pt.Integral(binidx,h_pt.GetNbinsX()+1)
        g_xsec.SetPoint(gidx,pt,xsec)
        gidx+=1
        print(pt,xsec)
    return g_xsec

def main():
    filetools.filemap('test1.root','ggf')

    #
    # Top mass effects
    g_xsec=make_xsecgraph('ggf:/nominal')
    g_xsec_nnlops=make_xsecgraph('ggf:/nnlops_nominal')
    g_xsec_eft=make_xsecgraph('ggf:/mtinf')

    plottools.graphs([(g_xsec,{'title':'MiNLO'}),
                      (g_xsec_nnlops,{'title':'NNLOPS','color':ROOT.kRed}),
                      (g_xsec_eft,{'title':'EFT','color':ROOT.kBlue})],
                     opt='C',
                     linewidth=2,
                     xtitle='p^{H}_{T} [GeV]',xrange=(0,1000),
                     ytitle='#sigma(p_{T}>p_{T}^{H}) #times BR [pb]',yrange=(1e-4,50),logy=True,
                     text='ggF H#rightarrowbb',textpos=(0.55,0.75),
                     legend=(0.6,0.7))
    canvastools.save('xsec_approx.pdf')

    #
    # Scale uncertainty
    g_xsec_muR05muF05=make_xsecgraph('ggf:/muR05muF05')
    g_xsec_muR05muF10=make_xsecgraph('ggf:/muR05muF10')
    g_xsec_muR10muF05=make_xsecgraph('ggf:/muR10muF05')
    g_xsec_muR10muF10=make_xsecgraph('ggf:/nominal')
    g_xsec_muR10muF20=make_xsecgraph('ggf:/muR10muF20')
    g_xsec_muR20muF10=make_xsecgraph('ggf:/muR20muF10')
    g_xsec_muR20muF20=make_xsecgraph('ggf:/muR20muF20')

    plottools.graphs([(g_xsec,{'title':'Nominal'}),
                      (g_xsec_muR05muF05,{'title':'#mu_{R}#times0.5, #mu_{F}#times0.5','color':ROOT.Blind1}),
                      (g_xsec_muR05muF10,{'title':'#mu_{R}#times0.5, #mu_{F}#times1.0','color':ROOT.Blind2}),
                      (g_xsec_muR10muF05,{'title':'#mu_{R}#times1.0, #mu_{F}#times0.5','color':ROOT.Blind3}),
                      (g_xsec_muR10muF20,{'title':'#mu_{R}#times1.0, #mu_{F}#times2.0','color':ROOT.Blind4}),
                      (g_xsec_muR20muF10,{'title':'#mu_{R}#times2.0, #mu_{F}#times1.0','color':ROOT.Blind5}),
                      (g_xsec_muR20muF20,{'title':'#mu_{R}#times2.0, #mu_{F}#times2.0','color':ROOT.Blind6})],
                     opt='C',
                     linewidth=2,
                     ratio=0,ratiorange=(0.5,1.5),
                     xtitle='p^{H}_{T} [GeV]',xrange=(0,1000),
                     ytitle='#sigma(p_{T}>p_{T}^{H}) #times BR [pb]',yrange=(1e-4,50),logy=True,
                     text='ggF H#rightarrowbb, Hj+MiNLO',textpos=(0.55,0.8),
                     legend=(0.45,0.75),legendncol=2)
    canvastools.save('xsec_mu.pdf')
    

    #
    # PDF Uncertainty
    
    g_xsec_pdfs=[(g_xsec,{'title':'NNPDF30_nnlo_as_0118'})]
    for i in range(0,101):
        g_xsec_pdfs.append((make_xsecgraph('ggf:/NNPDF30_nlo_as_0118_{:03d}'.format(i)),{'title':'NNPDF30_nlo_as_0118' if i==0 else '','color':ROOT.Blind1}))

    for i in range(0,33):
        g_xsec_pdfs.append((make_xsecgraph('ggf:/PDF4LHC15_nlo_30_pdfas_{:03d}'.format(i)),{'title':'PDF4LHC15_nlo_30_pdfas' if i==0 else '','color':ROOT.Blind2}))

    g_xsec_pdfs.append((make_xsecgraph('ggf:/MMHT2014nlo68clas118'),{'title':'MMHT2014nlo68clas118','color':ROOT.Blind3}))
    g_xsec_pdfs.append((make_xsecgraph('ggf:/CT14nlo'),{'title':'CT14nlo','color':ROOT.Blind4}))

    plottools.graphs(g_xsec_pdfs,
                     opt='C',
                     linewidth=2,
                     ratio=0,ratiorange=(0.7,1.3),
                     xtitle='p^{H}_{T} [GeV]',xrange=(0,1000),
                     ytitle='#sigma(p_{T}>p_{T}^{H}) #times BR [pb]',yrange=(1e-4,50),logy=True,
                     text='ggF H#rightarrowbb, PDF Variations',textpos=(0.55,0.8),
                     legend=(0.5,0.75))
    canvastools.save('xsec_pdf.pdf')
