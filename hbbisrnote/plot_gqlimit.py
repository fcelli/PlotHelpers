from prot import filetools
from prot import plottools
from prot import canvastools
from prot import textools


from hbbisrnote import constants

import pandas as pd
import numpy as np

import ROOT

def main():
    limits = pd.read_csv('hbbisrnote/data/zplimits.csv', sep='\t')

    limits['gqobs']=np.sqrt(limits['obs']/limits['theory']*(0.25**2))
    limits['gqexp']=np.sqrt(limits['exp']/limits['theory']*(0.25**2))

    limits['gq1sigup']=np.sqrt((limits['exp']+limits['1sigup'])/limits['theory']*(0.25**2))-limits['gqexp']
    limits['gq1sigdw']=np.sqrt((limits['exp']+limits['1sigdw'])/limits['theory']*(0.25**2))-limits['gqexp']
    limits['gq2sigup']=np.sqrt((limits['exp']+limits['2sigup'])/limits['theory']*(0.25**2))-limits['gqexp']
    limits['gq2sigdw']=np.sqrt((limits['exp']+limits['2sigdw'])/limits['theory']*(0.25**2))-limits['gqexp']

    print(limits)    

    g_exp1=ROOT.TGraphAsymmErrors()
    g_exp2=ROOT.TGraphAsymmErrors()
    g_obs =ROOT.TGraph()

    gidx=0
    for idx,limit in limits.iterrows():
        print(limit['mR'],limit['2sigdw'])
        
        g_exp1.SetPoint(gidx,limit['mR'],limit['gqexp'])
        g_exp1.SetPointEYhigh(gidx,limit['gq1sigup'])
        g_exp1.SetPointEYlow (gidx,limit['gq1sigdw'])

        g_exp2.SetPoint(gidx,limit['mR'],limit['gqexp'])
        g_exp2.SetPointEYhigh(gidx,limit['gq2sigup'])
        g_exp2.SetPointEYlow (gidx,limit['gq2sigdw'])

        g_obs .SetPoint(gidx,limit['mR'],limit['gqobs'])

        gidx+=1

    canvastools.canvas().SetCanvasSize(698,670)
    plottools.graphs([(g_exp2,{'title':'Expected #pm2#sigma',
                               'opt':'3',
                               'fillcolor':ROOT.kYellow,
                               'linestyle':0,
                               'markerstyle':0}),
                      (g_exp1,{'title':'Expected #pm1#sigma',
                               'opt':'3',
                               'fillcolor':ROOT.kGreen,
                               'linestyle':0,
                               'markerstyle':0}),
                      (g_exp1,{'title':'Expected',
                               'opt':'LX',
                               'linestyle':ROOT.kDashed,
                               'linewidth':2,
                               'markerstyle':0}),
                      (g_obs,{'title':'Observed',
                              'opt':'PL',
                              'linestyle':ROOT.kSolid,
                              'linewidth':2})],
                     xrange=(100,200),xtitle='m_{Z\'} [GeV]',
                     yrange=(0,0.4),ytitle='g_{q}',
                     text='',lumi=constants.lumi,textpos=(0.2,0.8),sim=False,
                     legend=(0.2,0.8))

    canvastools.save('gqlimit.pdf')
