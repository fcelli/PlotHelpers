import ROOT
import re

from prot import utiltools
from prot import filetools
from prot import plottools
from prot import canvastools
from prot import textools
from prot import style

import selectiontools
import optimtools

from hbbisrnote import constants

from math import *

def main(**kwargs):
    canvastools.savesetup('soversqrtb',**kwargs)

    filetools.filemap('OUT_fatjet_mc/hist-sig.root','sig')
    filetools.filemap('OUT_fatjet_mc/hist-sig-nofilt.root','sig-nofilt')
    filetools.filemap('OUT_fatjet_mc/hist-qcd.root','bkg')

    selectiontools.loop(optimtools.runAllCutStudy,'bkg','sig'       ,selection='hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt*_tjet2_*sort0_vetottbarCR_hpt*_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77',massdir='M100to150/',**kwargs)

    selectiontools.loop(optimtools.runAllCutStudy,'bkg','sig-nofilt',selection='hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt250_fj1pt*_tjet2_*sort0_vetottbarCR_hpt*_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77',massdir='M100to150/',**kwargs)

    canvastools.savereset()
