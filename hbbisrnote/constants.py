from prot import filetools

lumi15=3.2
lumi16=33.0
lumi17=44.3
lumi1516=lumi15+lumi16
lumi=lumi15+lumi16+lumi17

qcdscale=0.753763 #   +/-   0.000265599

selectionSR='hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_vetottbarCR_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77'
selectionCR='hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_vetottbarCR_hpt480_hm40_nbtag0in2ext_MV2c10_FixedCutBEff_85'
selectionVR='hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_vetottbarCR_hpt480_hm40_nlnotbtag1in2_MV2c10_FixedCutBEff_77'
selectionTTbarCR='hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_ttbarCR_hpt480_hm40_nbtag1in2ext_MV2c10_FixedCutBEff_77'

selectiontitles={}
selectiontitles['hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_vetottbarCR_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77']='SR'
selectiontitles['hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_vetottbarCR_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77']='SR'

selectiontitles['hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_vetottbarCR_hpt480_hm40_nlnotbtag1in2_MV2c10_FixedCutBEff_77']='VR'
selectiontitles['hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_vetottbarCR_hpt480_nlnotbtag1in2_MV2c10_FixedCutBEff_77']='VR'

selectiontitles['hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_vetottbarCR_hpt480_hm40_nbtag0in2ext_MV2c10_FixedCutBEff_85']='CR'
selectiontitles['hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_vetottbarCR_hpt480_nbtag0in2ext_MV2c10_FixedCutBEff_85']='CR'

selectiontitles['hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet1ext_ptsort0_vetottbarCR_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77']='SR - only 1 trkjet largeR jets'
selectiontitles['hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2al1_ptsort0_vetottbarCR_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77']='SR - allow 1 trkjet largeR jets'

#
# Zprime signals
def Zpfile(Zpmass):
    massstr='mR{}'.format(Zpmass)
    return massstr

Zpmasses=[100,125,150,175,200,250]

for Zpmass in Zpmasses:
    infile='hist-mc16_13TeV.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350P100_mRp{}_gSp25.root'.format(str(Zpmass).replace('0',''))
    massstr='mR{}'.format(Zpmass)
    filetools.filemap('OUT_fatjet_mc/{}'.format(infile),massstr)

