import re

common_titles={'mu': '< #mu >',
               'pt': 'P_{T}',
               'eta': '|#eta|',
               'iso': '#DeltaR_{closest jet > 15 GeV}'}

common_units={'mu': None,
              'pt': 'GeV',
              'eta': None,
              'iso': None}

def extract_info(name):
    re_code=re.compile(r'[a-zA-Z]+[0-9\.]*to[0-9\.]*')
    re_num=re.compile(r'[0-9]+')
    parts=name.split('_')
    bins=[]
    varname=[]
    for part in parts:
        if re_code.search(part)!=None and re_num.search(part)!=None:
            bin=Bin()
            bin.from_code(part)
            bins.append(bin)
        else: varname.append(part)

    return bins,'_'.join(varname)

def make_info(bins,varname):
    bininfo='_'.join([bin.to_code() for bin in bins])
    name='%s_%s'%(bininfo,varname)
    return name

def from_code(code):
    thebin=Bin()
    thebin.from_code(code)
    return thebin

class Bin(object):
    def __init__(self,name=None,minval=None,maxval=None,format='%0.1f',branch=None):
        self.name=name
        self.minval=minval
        self.maxval=maxval

        self.format=format

        self.branch=branch if branch!=None else name

    def contains(self,value):
        if self.minval!=None and value<self.minval: return False
        if self.maxval!=None and value>=self.maxval: return False
        return True

    def from_code(self,code):
        re_code=re.compile(r'([a-zA-Z]+)([0-9\.]*)to([0-9\.]*)')
        res=re_code.search(code)

        self.name=res.group(1)
        self.minval=float(res.group(2)) if res.group(2)!='' else None
        self.maxval=float(res.group(3)) if res.group(3)!='' else None

        example=res.group(2) if res.group(2)!='' else res.group(3)
        parts=example.split('.')
        if len(parts)==1: self.format='%d'
        else:
            self.format='%%0.%df'%len(parts[1])

    def midpoint(self):
        if self.minval==None:
            return self.maxval
        elif self.maxval==None:
            return self.minval
        else:
            return (self.minval+self.maxval)/2

    def midpoint_bounds(self):
        mp=self.midpoint()
        if self.minval==None:
            return mp,self.maxval-mp
        elif self.maxval==None:
            return self.minval-mp,0.1*mp
        else:
            return mp-self.minval,self.maxval-mp
        return 0,0

    def to_code(self):
        minval=(self.format%self.minval) if self.minval!=None else ''
        maxval=(self.format%self.maxval) if self.maxval!=None else ''
        return '%s%sto%s'%(self.name,minval,maxval)

    def to_title(self):
        title=common_titles.get(self.name,self.name)
        units=' '+common_units.get(self.name,'') if common_units[self.name]!=None else ''
        minval=(self.format+'%s < ')%(self.minval,units) if self.minval!=None else ''
        maxval=' < '+self.format%self.maxval+units if self.maxval!=None else ''
        return '%s%s%s'%(minval,title,maxval)

    def to_selection(self):
        selection=[]
        if self.minval!=None: selection+=['%f < %s'%(self.minval,self.branch)]
        if self.maxval!=None: selection+=['%s < %f'%(self.branch,self.maxval)]
        return ' && '.join(selection)
    
    def __lt__(self, other):
        if self.name!=other.name: raise Exception
        return self.maxval!=None and other.minval!=None and self.maxval <= other.minval
#    def __gt__(self, other):
#        if self.name!=other.name: raise Exception
#        return self.maxval!=None and other.minval!=None and self.maxval < other.minval        
    def __eq__(self, other):
        if self.name!=other.name: raise Exception
        return self.maxval==other.maxval and self.minval==other.minval
#    def __le__(self, other):
#        return mycmp(self.obj, other.obj) <= 0
#    def __ge__(self, other):
#        return mycmp(self.obj, other.obj) >= 0
    def __ne__(self, other):
        if self.name!=other.name: raise Exception
        return self.maxval!=other.maxval or self.minval!=other.minval
    def __hash__(self):
        return hash(self.to_code())

class Category(object):
    def __init__(self,name=None,value=None,branch=None):
        self.name=name
        self.value=value

        self.branch=branch if branch!=None else name

    def contains(self,value):
        return self.value==value

    def to_code(self):
        return self.value

    def to_title(self):
        return self.value.title().replace('Quark','Light Quark')

    def to_selection(self):
        return '%s == "%s"'%(self.branch,self.value)
    
    def __lt__(self, other):
        return self.value<other.value
#    def __gt__(self, other):
#        if self.name!=other.name: raise Exception
#        return self.maxval!=None and other.minval!=None and self.maxval < other.minval        
#    def __eq__(self, other):
#        return mycmp(self.obj, other.obj) == 0
#    def __le__(self, other):
#        return mycmp(self.obj, other.obj) <= 0
#    def __ge__(self, other):
#        return mycmp(self.obj, other.obj) >= 0
#    def __ne__(self, other):
#        return mycmp(self.obj, other.obj) != 0


class BinStr(object):
    def __init__(self,name=None):
        self.name=name

    def contains(self,value):
        return name==value

    def from_code(self,code):
        self.name=code
        
    def to_code(self):
        return self.name
        
    def to_title(self):
        return self.name
