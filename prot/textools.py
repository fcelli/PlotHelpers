def sanitizestring(string):
    string=string.replace('_','')
    string=string.replace('/','')
    string=string.replace('.','p')
    string=string.replace('0','Zero')
    string=string.replace('1','One')
    string=string.replace('2','Two')
    string=string.replace('3','Three')
    string=string.replace('4','Four')
    string=string.replace('5','Five')
    string=string.replace('6','Six')
    string=string.replace('7','Seven')
    string=string.replace('8','Eight')
    string=string.replace('9','Nine')

    return string

def write_command(fh,name,value,fmt=''):
    outstring='\\newcommand{{\{}}}{{{%s}}} %% {}\n'%fmt
    fh.write(outstring.format(sanitizestring(name),value,name))
