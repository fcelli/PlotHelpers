import ROOT

from prot import utiltools
from prot import style
from prot import canvastools
from prot import plottools

def treeGraph(treeName,
              xbranch,
              ybranch,
              selection,
              **kwargs):

    tree=utiltools.Get(treeName)

    if tree==None:
        print('Error: Missing tree %s!'%treeName)
        return None

    # Make the graph
    n=tree.Draw('%s:%s'%(ybranch,xbranch),selection);
    g=ROOT.TGraph(n,tree.GetV2(),tree.GetV1());
    style.style.apply_style(g,kwargs,'treeGraph')

    return g

def treePlot(treeName,
             xbranch, xnbins, xminval, xmaxval,
             selection,
             histName,
             **kwargs):

    tree=utiltools.Get(treeName)
    if tree==None:
        print("Missing tree",treeName,"!")
        return None

    # Build up the draw string
    drawStr="%s>>%s(%d,%f,%f)"%(xbranch,histName,
                                xnbins,xminval,xmaxval)
    ROOT.gROOT.cd()
    tree.Draw(drawStr,selection)

    plottools.plot(histName,**kwargs);
    return ROOT.gROOT.Get(histName)

def treePlot2D(treeName,
               xbranch, xnbins, xminval, xmaxval,
               ybranch, ynbins, yminval, ymaxval,
               selection,
               histName,
               **kwargs):

    tree=utiltools.Get(treeName)
    if tree==None:
        print("Missing tree",treeName,"!")
        return None

    # Build up the draw string
    drawStr="%s:%s>>%s(%d,%f,%f,%d,%f,%f)"%(ybranch,xbranch,histName,
                                            xnbins,xminval,xmaxval,
                                            ynbins,yminval,ymaxval)
    ROOT.gROOT.cd()
    tree.Draw(drawStr,selection,"COL")

    plottools.plot2d(histName,**kwargs);
    return ROOT.gROOT.Get(histName)
