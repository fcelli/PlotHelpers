from prot import canvastools
from prot import filetools
from prot import plottools
from prot import triggertools

import ROOT

import re

re_nametotitle=re.compile('gammajet_g([0-9]+)_j([0-9]+)_.*')

def nametotitle(name):
    match=re_nametotitle.match(name)
    gpt=match.group(1)
    jpt=match.group(2)
    return 'p_{T,#gamma}>%s GeV, p_{T,jet}>%s GeV'%(gpt,jpt)

def main():
    filetools.open('OUT_photontrigger_data/hist16.root')
    filetools.open('OUT_photontrigger_mc/hist.root')

    data=triggertools.trigeff('_file0:/gammajet_g25_j25_HLT_g140_tight/photon0_pt','_file0:/gammajet_g25_j25_HLT_g140_tight_ref/photon0_pt')
    mc  =triggertools.trigeff('_file1:/gammajet_g25_j25_HLT_g45_tight/photon0_pt','_file1:/gammajet_g25_j25_HLT_g45_tight_ref/photon0_pt')

    plottools.graphs([(mc.CreateGraph(),{'title':'Sherpa #gamma+jet (HLT_g45_tight)','color':ROOT.kRed,'marker':ROOT.kOpenCircle}),
                      (data.CreateGraph(),{'title':'Data (HLT_g140_tight)'})],
                      xrange=(0,300),yrange=(0.9,1.05),legend=(0.53,0.93),
                      lumi=32.9,sim=False,textpos=(0.2,0.8),text=nametotitle('gammajet_g25_j25_HLT_g140_tight'))

    l=ROOT.TLine(0,1,300,1)
    l.SetLineStyle(ROOT.kDashed)
    l.Draw()
    
    canvastools.save('photontrigger/singlephoton/zoom_photon0_pt.pdf')
