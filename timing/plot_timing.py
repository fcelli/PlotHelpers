import glob

from prot import filetools
from prot import plottools
from prot import canvastools

import ROOT

def make_hists(dir):
    print(dir)
    hcpu =ROOT.TH1F('','',100,0,0.001)
    hreal=ROOT.TH1F('','',100,0,0.001)
    for path in sorted(glob.glob('{}/fetch/hist-*'.format(dir))):
        fh=ROOT.TFile.Open(path)
        h=fh.Get('EventLoop_RunTime')
        if h==None:
            fh.Close()
            continue
        events  =h.GetBinContent(3)
        cputime =h.GetBinContent(4)
        realtime=h.GetBinContent(5)
        if events==0:
            fh.Close()
            continue
        fh.Close()

        hcpu .Fill(cputime /events)
        hreal.Fill(realtime/events)
    plottools.plots([hcpu,hreal],hsopt='nostack')
    canvastools.save('{}.pdf'.format(dir))
    return hcpu,hreal
    
def main():
    hcpuh32,hrealh32=make_hists('OUT_fatjet_data_haswell32')
    hcpuh64,hrealh64=make_hists('OUT_fatjet_data_haswell64')
    hcpuk34,hrealk34=make_hists('OUT_fatjet_data_knl34')
    hcpuk64,hrealk64=make_hists('OUT_fatjet_data_knl64')
    hcpuk68,hrealk68=make_hists('OUT_fatjet_data_knl68')
    hcpuk128,hrealk128=make_hists('OUT_fatjet_data_knl128')
    hcpuk136,hrealk136=make_hists('OUT_fatjet_data_knl136')
    hcpuk256,hrealk256=make_hists('OUT_fatjet_data_knl256')
    hcpuk272,hrealk272=make_hists('OUT_fatjet_data_knl272')
    #plottools.plots([hcpuh64,hcpuk64],hsopt='nostack')

    gh=ROOT.TGraph()
    gidx=0    
    gh.SetPoint(gidx,32,hrealh32.GetMean()/32)
    gidx+=1
    gh.SetPoint(gidx,64,hrealh64.GetMean()/64)

    gk=ROOT.TGraph()
    gidx=0
    gk.SetPoint(gidx,34,hrealk34.GetMean()/34)
    gidx+=1    
    gk.SetPoint(gidx,68,hrealk68.GetMean()/68)
    gidx+=1
    gk.SetPoint(gidx,64,hrealk64.GetMean()/64)
    gidx+=1
    gk.SetPoint(gidx,128,hrealk128.GetMean()/128)
    gidx+=1
    gk.SetPoint(gidx,136,hrealk136.GetMean()/136)
    gidx+=1
    gk.SetPoint(gidx,256,hrealk256.GetMean()/256)
    gidx+=1
    gk.SetPoint(gidx,272,hrealk272.GetMean()/272)

    plottools.graphs([  (gh,{'title':'Haswell','color':ROOT.kBlue,'markerstyle':ROOT.kFullCircle}),
                        (gk,{'title':'KNL','color':ROOT.kRed,'markerstyle':ROOT.kFullSquare})
                          ],
                          opt='LP',linestyle=ROOT.kDashed,
                          xtitle='Processes per Node',
                          ytitle='Real Time Per Event / Processes per Node [s]',
                          
                         )
