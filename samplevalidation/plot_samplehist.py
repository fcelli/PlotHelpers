import glob
import re
import os, os.path

import ROOT

from prot import plottools
from prot import filetools
from prot import canvastools

def main(sampleDir, selection, run):
    # create plots
    heff     =ROOT.TH2F("heff"     ,"Sample Efficiency;m_{R} [GeV];g_{SM}"  ,10,50,1050,9,0.05,0.95)
    hrmsfrac =ROOT.TH2F("hrmsfrac" ,"RMS[m_{jj}]/<m_{R}>;m_{R} [GeV];g_{SM}",10,50,1050,9,0.05,0.95)
    hrms     =ROOT.TH2F("hrms"     ,"RMS[m_{jj}] [GeV];m_{R} [GeV];g_{SM}"  ,10,50,1050,9,0.05,0.95)
    hmean    =ROOT.TH2F("hmean"    ,"<m_{jj}> [GeV];m_{R} [GeV];g_{SM}"     ,10,50,1050,9,0.05,0.95)
    htrmsfrac=ROOT.TH2F("htrmsfrac","RMS[m_{R}]/<m_{R}>;m_{R} [GeV];g_{SM}" ,10,50,1050,9,0.05,0.95)
    htrms    =ROOT.TH2F("htrms"    ,"RMS[m_{R}] [GeV];m_{R} [GeV];g_{SM}"   ,10,50,1050,9,0.05,0.95)
    htmean   =ROOT.TH2F("htmean"   ,"<m_{R}> [GeV];m_{R} [GeV];g_{SM}"      ,10,50,1050,9,0.05,0.95)
    
    # make mappings
    #re_histpath=re.compile('hist-MC15.[0-9]+.MGPy8EG_dmA.*_mR([0-9]+)_mDM([0-9]+)_gSM([0-9]+p[0-9]+)_gDM([0-9]+p[0-9]+).NTUP.root')
    re_histpath=re.compile('hist-MC15.[0-9]+.MGPy8EG_dmA.*_Jet350_mR([0-9]+)_mDM([0-9]+)_gSM([0-9]+p[0-9]+)_gDM([0-9]+p[0-9]+).NTUP.root')

    points=[]
    histpaths=glob.glob('%s/hist-MC15.%d.MGPy8EG_dmA_*.NTUP.root'%(sampleDir,run))
    for histpath in histpaths:
        match=re_histpath.match(os.path.basename(histpath))
        if match==None: continue

        mR =int(match.group(1))
        mDM=int(match.group(2))
        gSM=float(match.group(3).replace('p','.'))
        gDM=float(match.group(4).replace('p','.'))

        name='sig_mR%d_%0.2f'%(mR,gSM)
        filetools.filemap(histpath,name)
        points.append((mR,gSM))

    # Loop over points
    for mR,gSM in points:
        # General things
        name='sig_mR%d_%0.2f'%(mR,gSM)
        binidx=heff.FindBin(mR,gSM);
        fh=filetools.open(name)

        # Efficiency things
        cutflow=fh.Get('%s/cutflow'%selection)
        eff=cutflow.GetBinContent(9)
        heff.SetBinContent(binidx,eff);

        # Peak things
        hmjj=fh.Get("%s/Zprime_mjj"%selection);

        hmean   .SetBinContent(binidx,hmjj.GetMean());
        hrms    .SetBinContent(binidx,hmjj.GetRMS());
        hrmsfrac.SetBinContent(binidx,hmjj.GetRMS()/hmjj.GetMean());

        # Truth things things
        hmR=fh.Get("%s/ZprimeTruth_zm"%selection);

        htmean   .SetBinContent(binidx,hmR.GetMean());
        htrms    .SetBinContent(binidx,hmR.GetRMS());
        htrmsfrac.SetBinContent(binidx,hmR.GetRMS()/hmR.GetMean());


    # Plot it all!
    ROOT.gStyle.SetPaintTextFormat("0.2f")
    ROOT.gROOT.cd();
    plottools.plot2d(heff,
                     opt='COLZTEXT',
                     zrange=(0,1),
                     text='Selection Efficiency for %s'%selection,
                     textpos=(0.00,0.04))
    canvastools.save("sample_%s_eff.svg"%selection);

    plottools.plot2d(hmean,
                     opt='COLZTEXT',
                     zrange=(100,1000),
                     text='<m_{jj}> for %s'%selection,
                     textpos=(0.00,0.04))
    canvastools.save("sample_%s_mean.svg"%selection);

    plottools.plot2d(hrms,
                     opt='COLZTEXT',
                     zrange=(100,400),
                     text='RMS[m_{jj}] for %s'%selection,
                     textpos=(0.00,0.04))
    canvastools.save("sample_%s_rms.svg"%selection);

    plottools.plot2d(hrmsfrac,
                     opt='COLZTEXT',
                     zrange=(0,1),
                     text='RMS[m_{jj}]/<m_{jj}> for %s'%selection,
                     textpos=(0.00,0.04))
    canvastools.save("sample_%s_rmsfrac.svg"%selection);

    plottools.plot2d(htmean,
                     opt='COLZTEXT',
                     zrange=(100,1000),
                     text='<m_{R}> for %s'%selection,
                     textpos=(0.00,0.04))
    canvastools.save("sample_%s_truthmean.svg"%selection);

    plottools.plot2d(htrms,
                     opt='COLZTEXT',
                     zrange=(100,400),
                     text='RMS[m_{R}] for %s'%selection,
                     textpos=(0.00,0.04))
    canvastools.save("sample_%s_truthrms.svg"%selection);

    plottools.plot2d(htrmsfrac,
                     opt='COLZTEXT',
                     zrange=(0,1),
                     text='RMS[m_{R}]/<m_{R}> for %s'%selection,
                     textpos=(0.00,0.04))
    canvastools.save("sample_%s_truthrmsfrac.svg"%selection);    

    # Plot information
    gSMs=[0.9] #[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
    for gSM in gSMs:
        filelist=[#('sig_mR100_%0.2f'%(gSM),{'title':'100 GeV','color':ROOT.kRed}),
                          ('sig_mR300_%0.2f'%(gSM),{'title':'300 GeV','color':ROOT.kBlack}),
                          ('sig_mR500_%0.2f'%(gSM),{'title':'500 GeV','color':ROOT.kBlue}),
                          ('sig_mR700_%0.2f'%(gSM),{'title':'700 GeV','color':ROOT.kGreen+3}),
                          ('sig_mR900_%0.2f'%(gSM),{'title':'900 GeV','color':ROOT.kOrange})]
        plottools.plotsf(filelist,
                          '%s/Zprime_mjj'%selection,
                          logy=True,normalize=True,hsopt='nostack',xrange=(0,1000),opt='hist',
                          legend=(0.2,0.35),legendncol=2,legendwidth=0.2,
                          text='%s\ng_{SM}=%0.1f'%(selection,gSM),textpos=(0.52,0.75))
        canvastools.save("Zprime_mjj_gSM%0.2f.pdf"%gSM)

        plottools.plotsf(filelist,
                          '%s/ZprimeTruth_zm'%selection,
                          logy=True,normalize=True,hsopt='nostack',xrange=(0,1000),opt='hist',
                          legend=(0.2,0.35),legendncol=2,legendwidth=0.2,
                          text='%s\ng_{SM}=%0.1f'%(selection,gSM),textpos=(0.52,0.75))
        canvastools.save("Zprime_mR_gSM%0.2f.pdf"%gSM)

        plottools.plotsf(filelist,
                          '%s/jet0_pt_m'%selection,
                          logy=True,normalize=True,hsopt='nostack',xrange=(400,1000),opt='hist',
                          legend=(0.2,0.35),legendncol=2,legendwidth=0.2,
                          text='%s\ng_{SM}=%0.1f'%(selection,gSM),textpos=(0.52,0.75))
        canvastools.save("jet0_pt_gSM%0.2f.pdf"%gSM)

        plottools.plotsf(filelist,
                          '%s/jet1_pt_l'%selection,
                          logy=True,normalize=True,hsopt='nostack',xrange=(0,1500),opt='hist',
                          legend=(0.22,0.35),legendncol=2,legendwidth=0.2,
                          text='%s\ng_{SM}=%0.1f'%(selection,gSM),textpos=(0.5,0.75))
        canvastools.save("jet1_pt_gSM%0.2f.pdf"%gSM)

        plottools.plotsf(filelist,
                          '%s/jet2_pt_l'%selection,
                          logy=True,normalize=True,hsopt='nostack',xrange=(0,1500),opt='hist',
                          legend=(0.45,0.68),legendncol=2,legendwidth=0.2,
                          text='%s\ng_{SM}=%0.1f'%(selection,gSM),textpos=(0.5,0.75))
        canvastools.save("jet2_pt_gSM%0.2f.pdf"%gSM)
        
