import ROOT
from prot import plottools

import dftools

import sys

def df_indexlist(df,branch):
    """ Create a column containing indexes 0..nbranch. """
    return df.Define('{branch}_idx'.format(branch=branch),'std::vector<uint32_t> {branch}_idx(n{branch}); std::iota({branch}_idx.begin(),{branch}_idx.end(),0); return {branch}_idx;'.format(branch=branch))

def df_filterlist(df,inlist,outlist,filtercode):
    return df.Define('{}_idx'.format(outlist),'std::vector<uint32_t> {outlist}_idx; std::copy_if({inlist}_idx.begin(),{inlist}_idx.end(),std::back_inserter({outlist}_idx), {filtercode}); return {outlist}_idx;'.format(inlist=inlist,outlist=outlist,filtercode=filtercode))

def df_decoratelist(df,inlist,decorname,decortype,decorcode):
    return df.Define('{}_{}'.format(inlist,decorname),'std::vector<{decortype}> {inlist}_{decorname}; std::transform({inlist}_idx.begin(),{inlist}_idx.end(),std::back_inserter({inlist}_{decorname}), {decorcode}); return {inlist}_{decorname};'.format(inlist=inlist,decorname=decorname,decortype=decortype,decorcode=decorcode))


class DFAnalysis():
    def __init__(self,sample):
        self.finaldf=[]
        
        self.rootdf=None
        if 'dir' in sample:
            self.rootdf=dftools.makedf_dir(sample.get('dir'),sample.get('dirfilter','*.root'),lumi=sample.get('lumi'),weighted=sample.get('weighted',True))
        elif 'filelist' in sample:
            self.rootdf=dftools.makedf_filelist(sample.get('filelist'),lumi=sample.get('lumi'),weighted=sample.get('weighted',True))
        else:
            print('ERROR: Unknown sample specification!')
            return

        if self.rootdf!=None: 
            self.selection(self.rootdf)

    def histogram(self,dfname,var,nbins,binmin,binmax,weight='w'):
        h=None
        if weight!=None:
            h=getattr(self,dfname).Histo1D(('','',nbins,binmin,binmax),var,'w')
        else:
            h=getattr(self,dfname).Histo1D(('','',nbins,binmin,binmax),var)
        return h.GetValue()

    def cutflow(self,dfnames,weight='w'):
        cutflow=[]
        for dfname in dfnames:
            if weight!=None:
                cutflow.append(getattr(self,dfname).Sum('w').GetValue())
            else:
                cutflow.append(getattr(self,dfname).Count().GetValue())
        return cutflow


class MultiDFAnalysis():
    def __init__(self,anaclass,samples,**kwargs):
        self.samples={}
        for sample in samples:
            dfana=anaclass(sample,**kwargs)
            self.samples[sample['name']]=dfana

    def histogram(self,dfname,var,nbins,binmin,binmax,weight='w'):
        hist=None
        for sample in self.samples.values():
            if sample.rootdf==None: continue
            if weight!=None:
                h=getattr(sample,dfname).Histo1D(('','',nbins,binmin,binmax),var,weight)
            else:
                h=getattr(sample,dfname).Histo1D(('','',nbins,binmin,binmax),var)
            if hist==None:
                hist=h.GetValue().Clone()
            else:
                hist.Add(h.GetValue())
        return hist

    def cutflow(self,dfnames,weight='w'):
        cutflow=None
        for sample in self.samples.values():
            scutflow=sample.cutflow(dfnames,weight)
            if cutflow==None:
                cutflow=scutflow
            else:
                cutflow=[x[0]+x[1] for x in zip(cutflow,scutflow)]
        return cutflow
