import plot_datamc
import makeFitDataLike
import addDataHists
import runSingleFit
# import makeAcceptanceTable
# import plot_xseccomp
import plotGaussianLimits
import dibjet.runGaussian

from prot import style
from prot import utiltools

import ROOT

def runDataMC():
    #
    # Data vs MC comparisons
    oldstyledata=style.style.data
    style.style.parse('styles/variables.style')
    style.style.parse('styles/dibjetvariables.style')
    plot_datamc.main(("OUT_dibjet_mc/hist.root",{"title":"Pythia 8 Dijet"}),
                     ("OUT_dibjet_data/hist.root",{"title":"Data"}),
                     ("OUT_dibjet_mc/hist-group.phys-exotics:group.phys-exotics.mc15_13TeV.301921.Pythia8EvtGen_A14NNPDF23LO_Zprimebb800.ZPrimeBB_noTrig_20160513_tree.root.root",{"title":"m_{Z'}=800 GeV (x100)","scale":100,"linecolor":ROOT.kBlue}),
                     ratiorange=(0.5,1.5),ytitle="Events",text={"title":"","lumi":3.2,"sim":False},textpos=(0.68,0.8))
    style.style.data=oldstyledata

def runFits():
    addDataHists.main('OUT_dibjet_data')
    runSingleFit.main("OUT_dibjet_data/datalike.root",massRanges=[570,1200])

def runFitDatalike():
    makeFitDataLike.main("OUT_dibjet_data/hist.root")
    dibjet.runGaussian.main("OUT_dibjet_data/datalike.root")
    plotGaussianLimits.main([("OUT_dibjet_data/limits570to1200_100.root",{"color":ROOT.kMagenta,
                                                                          "markerstyle":ROOT.kOpenCircle}),
                             ("OUT_dibjet_data/limits570to1200_50.root",{"color":ROOT.kGreen+2,
                                                                         "markerstyle":ROOT.kOpenSquare}),
                             ("OUT_dibjet_data/limits570to1200_30.root",{"color":ROOT.kRed,
                                                                         "markerstyle":ROOT.kOpenTriangleUp})])    
    
def main(doDataMC=False,doFits=False,doDataLike=False):
    #
    # Do data vs MC comparisons
    if doDataMC:
        runDataMC()

    #
    # Run fit tests
    if doFits:
        runFits()
        
    #
    # Papare datalike
    if doDataLike:
        runFitDatalike()

    # #
    # # Expected limits
    # dibjet.runGaussian.main("output500to1200.root")



    #
    # Cutflow
    #plots([("dibjet85/cutflow_weighted",{"title":"WP85"}),("dibjet77/cutflow_weighted",{"title":"WP77","color":ROOT.kRed}),("dibjet70/cutflow_weighted",{"title":"WP70","color":ROOT.kBlue}),("dibjet60/cutflow_weighted",{"title":"WP60","color":ROOT.kGreen+1})],opt="hist",hsopt="nostack",xtitle="Cut",ytitle="Acceptance",nounits=True,legend=(0.2,0.3),legendncol=2,legendwidth=0.15,text="Z#rightarrowbb m_{R}=800 GeV",textpos=(0.6,0.8))
