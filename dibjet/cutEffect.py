import ROOT

from prot import filetools
from prot import plottools
from prot import canvastools
from prot import style
import dijettools

def main(wp):
    filetools.open('OUT_dibjet_mc/hist.root')

    stages=[('dibjet%d_noystar_nopt_nobtag_notrig0_notrig1'%wp,{'title':'No Cuts'}),
            ('dibjet%d_nopt_nobtag_notrig0_notrig1'%wp,{'title':'Add y* cut'}),
            ('dibjet%d_nobtag_notrig0_notrig1'%wp,{'title':'Add p_{T} cuts'}),
            ('dibjet%d_notrig0_notrig1'%wp,{'title':'Add b-tagging'}),
            ('dibjet%d'%wp,{'title':'Add trigger SF'})]

    for i in range(len(stages)-1):
        plot0=stages[i]
        plot1=stages[i+1]

        plottools.plots([(plot0[0]+'/Zprime_mjj',plot0[1]),(plot1[0]+'/Zprime_mjj',plot1[1])],logy=True,xrange=(300,2000),legend=(0.6,0.3),ytitle='#sigma [pb / 1 GeV]',text='%s%% WP'%wp,rebin=dijettools.binsDijetDefault,nounits=True)
        canvastools.save('cuteffect/Zprime_mjj%d.pdf'%i)

        plottools.plots([(plot0[0]+'/jet0_pt',plot0[1]),(plot1[0]+'/jet0_pt',plot1[1])],logy=True,legend=(0.6,0.3),ytitle='#sigma [pb]',text='%s%% WP'%wp)
        canvastools.save('cuteffect/jet0_pt%d.pdf'%i)

        plottools.plots([(plot0[0]+'/jet1_pt',plot0[1]),(plot1[0]+'/jet1_pt',plot1[1])],logy=True,legend=(0.6,0.3),ytitle='#sigma [pb]',text='%s%% WP'%wp)
        canvastools.save('cuteffect/jet1_pt%d.pdf'%i)

    # total effect
    plottools.plots([(stages[0][0]+'/Zprime_mjj',{'title':'No Cuts'}),(stages[-1][0]+'/Zprime_mjj',{'title':'All Cuts'})],logy=True,xrange=(300,2000),legend=(0.7,0.9),ytitle='#sigma [pb / 1 GeV]',text='%s%% WP'%wp,rebin=dijettools.binsDijetDefault,nounits=True)
    canvastools.save('cuteffect/all.pdf')

    # global changes
    hists=[]
    i=0
    for stage in stages:
        stage[1]['color']=style.palette[i]
        hists.append((stage[0]+'/Zprime_mjj',stage[1]))
        i+=1        
    plottools.plots(hists,logy=True,xrange=(300,2000),legend=(0.6,0.95),ytitle='#sigma [pb / 1 GeV]',text='%s%% WP'%wp,rebin=dijettools.binsDijetDefault,nounits=True,hsopt='nostack hist')
    canvastools.save('cuteffect/global.pdf')

