from prot.utiltools import *
from prot.plottools import *
from prot.filetools import *
from prot.canvastools import *
from prot import batchtools

import ROOT

import selectiontools
import dataliketools
import runtools

import subprocess
import shutil
import glob
import os.path

def makeSysInput(d_sel,outDir='./',histname='Zprime_mjj',nameTag='',**kwargs):
    # output
    outDir =outDir+'/'+d_sel[0].GetName()
    outPath=outDir+'/'+nameTag+'.root'
    print(outPath)

    if not os.path.exists(outDir):
        os.makedirs(outDir)
    fh_out=filetools.open(outPath,'RECREATE')

    # names
    name='mjj_%s_1fb_Nominal'%nameTag
    hist=d_sel[0].Get(histname).Clone(name)
    hist.Scale(1e3)
    hist.Write()

    for key in d_sel[0].GetListOfKeys():
        if not key.GetName().startswith('sys') or not key.ReadObj().InheritsFrom(ROOT.TDirectoryFile.Class()): continue
        d_sys=key.ReadObj()

        name='mjj_%s_1fb_%s'%(nameTag,key.GetName()[3:])
        hist=d_sys.Get(histname).Clone(name)
        hist.Scale(1e3)
        hist.Write()

    fh_out.Close()


def main(inPath,outName='sys',histname='Zprime_mjj',nameTag='',**kwargs):
    bkgdir=os.path.dirname(inPath)

    outdir=bkgdir+'/'+outName
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    filename=os.path.basename(inPath)
    selectiontools.loop(makeSysInput,inPath,outDir=outdir,histname=histname,nameTag=nameTag,**kwargs)
