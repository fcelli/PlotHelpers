import ROOT

from prot import utiltools
from prot import filetools
from prot import plottools
from prot import canvastools
from prot import prettytools
from prot import histtools
from prot import style

import selectiontools

import shutil
import copy
import datetime
import os, os.path

def plotCompare(h0,h1,histname,**kwargs):
    # style requests
    stylename=histname.split('/')[-1]    
    
    #if 'jet1_eta' not in histname and 'jet1_phi' not in histname: return
    #if 'Zprime_' not in histname: return
    #if 'jet0_pt' not in histname or 'jet0_pt_' in histname: return
    if 'Mjj' in histname: return
    #if 'NumTrk' not in histname: return
    #if 'jet0_pt'!=stylename: return
    #if 'ZprimeISR_dRISRclosej'!=stylename: return

    if stylename in style.style.data: kwargs.update(style.style.data[stylename])

    #
    # Settings
    normalize=kwargs.get('normalize',False)
    logy=kwargs.get('logy',False)
    dijetard=kwargs.get('dijetard',False)
    xrange=style.torange(kwargs.get('xrange',(None,None)))
    
    #
    # Automatic pretty

    # Graph type
    direction='<'
    occupancy='<'
    minBin,maxBin=histtools.binrange(h0[0],*xrange)
    minBinAb0=max(minBin,h0[0].FindFirstBinAbove(0))

    occCentering=h0[0].Integral(minBin   ,int((minBin   +maxBin)/2)) / max(h0[0].Integral(int((minBin   +maxBin)/2),maxBin),1e-5)    
    if occCentering>1:
        occupancy='>'
    if abs(1-occCentering)<0.3:
        occupancy='='

    dirCentering=h0[0].Integral(minBinAb0,int((minBinAb0+maxBin)/2)) / max(h0[0].Integral(int((minBinAb0+maxBin)/2),maxBin),1e-5)
    if dirCentering>1:
        direction='>'
    if abs(1-dirCentering)<0.3:
        direction='='
    
    
    # Positions
    textpos=(0.6,0.8)
    legend=(0.17,0.93)
    if occupancy=='>':
        textpos=(0.6,0.8)
        legend=(0.6,0.8)
    elif occupancy=='=':
        textpos=(0.6,0.8)
        legend=(0.17,0.93)

    #
    # Determine yrange by looking at background
    scale=kwargs.get('rebin',1)
    if direction=='=': scale*=1.25
    if direction=='>' and logy and occCentering<2.5: scale*=1.75
    padratio=0.5 if logy else 0.1

    hmaxrange=h0[0].Clone()
    hmaxrange.Reset()

    hminrange=h0[0].Clone()
    hminrange.Reset()

    integral0=h0[0].Integral()
    if integral0==0: integral0=1.

    integral1=h1[0].Integral()
    if integral1==0: integral1=1.

    scale0=h0[1].get('scale',1) if not normalize else 1./integral0
    scale1=h1[1].get('scale',1) if not normalize else 1./integral1

    for binIdx in range(0,hmaxrange.GetNbinsX()+2):
        val0=h0[0].GetBinContent(binIdx)*scale0
        val1=h0[0].GetBinContent(binIdx)*scale1
        if val0>val1:
            hmaxrange.SetBinContent(binIdx,val0)
            hminrange.SetBinContent(binIdx,val1)
        else:
            hmaxrange.SetBinContent(binIdx,val1)
            hminrange.SetBinContent(binIdx,val0)
        
        if dijetard:
            hmaxrange.SetBinContent(binIdx,hmaxrange.GetBinContent(binIdx)/hmaxrange.GetBinWidth(binIdx))
            hminrange.SetBinContent(binIdx,hminrange.GetBinContent(binIdx)/hminrange.GetBinWidth(binIdx))

    # the actual range
    if logy:
        yrange=(prettytools.getminimum(hminrange,*xrange,direction=direction)[1]*(1-padratio)*scale,
                prettytools.getmaximum(hmaxrange,*xrange)[1]*(1+padratio)*scale)
    else:
        yrange=(0,
                prettytools.getmaximum(hmaxrange,*xrange)[1]*(1+padratio)*scale)

    print('direction',dirCentering,direction,yrange,1+padratio,scale)
    print('occupancy',occCentering,occupancy,yrange,1+padratio,scale)    
    
    #
    # Determine rebinning based on data
    newbins=None
    if logy and stylename!='Zprime_mjj_var':
        newbins=[]
        rebinNow=1
        rebinIdx=0
        maxdatabin,maxdata=histtools.getmaximum(h1[0])
        for binIdx in range(1,h1[0].GetNbinsX()+2):
            if rebinIdx==0:
                newbins.append(h1[0].GetBinLowEdge(binIdx))

                # Determine new binning
                nowdata=h1[0].GetBinContent(binIdx)
                rebinNow=min(int(maxdata/nowdata/10)+1 if nowdata>0 else rebinNow,4)
                rebinNow=rebinNow if binIdx>maxdatabin else 1
                rebinIdx=rebinNow-1
            else:
                rebinIdx-=1
        kwargs['rebin']=newbins

    # plot
    hlist=[]
    if h0[0]!=None: hlist.append(h0)
    if h1[0]!=None: hlist.append(h1)

    plottools.plots(hlist,
                    hsopt='nostack',
                    ratio=0,ratiolist=[0,1],yrange=yrange,legend=legend,textpos=textpos,**kwargs)
    canvastools.save('comp/%s.pdf'%os.path.basename(histname))

def compare(d0,d1,path='',**kwargs):
    oldsavepath=canvastools.savepath
    for key in d0[0].GetListOfKeys():
        name=key.GetName()
        newpath=path+'/'+name
        obj=key.ReadObj()

        allobjs=[(d0[0].Get(name),d0[1]),
                 (d1[0].Get(name),d1[1])]
        
        if obj.InheritsFrom(ROOT.TH1F.Class()):
            canvastools.savepath=oldsavepath
            plotCompare(allobjs[0],allobjs[1],newpath,**kwargs)
        elif obj.InheritsFrom(ROOT.TDirectoryFile.Class()):
            if obj.GetName().startswith('sys'): continue
            canvastools.savepath=oldsavepath+'/'+obj.GetName()            
            compare(allobjs[0],allobjs[1],newpath,**kwargs)
    canvastools.savepath=oldsavepath

def compare_selection(file0,file1,**kwargs):
    # start plotting
    compare(file0,file1,path=file0[0].GetName(),**kwargs)

def main(file0,file1,**kwargs):
    # Prepare inputs
    file0=utiltools.filetag(file0)
    file1=utiltools.filetag(file1)

    filetools.filemap(file0[0],'file0')
    filetools.filemap(file1[0],'file1')

    # Add lumi scaling
    lumi=kwargs.get('lumi',1.)
    file0[1]['scale']=file0[1].get('scale',1,)*lumi*1e3
    file1[1]['scale']=file1[1].get('scale',1,)*lumi*1e3

    #
    selectiontools.loop(compare_selection,file0,file1,**kwargs)

