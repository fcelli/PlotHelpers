#!/bin/bash

if [ ${#} == 0 ]; then
    echo "usage: ${0} output [output ...]"
    exit -1
fi

# ROOT
echo "Setting up Root ..."
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export ALRB_localConfigDir=${HOME}/localConfig
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet
lsetup fax root

# Merge
for OUTPUT in ${@}
do
    if [ -e ${OUTPUT} ]; then
	rm ${OUTPUT}
    fi

    hadd ${OUTPUT} ${OUTPUT}.*
    rm ${OUTPUT}.*
done
