#!/usr/bin/env python

############################################################
#
# singleFit.py
#
# Code for running a single fit.
#
############################################################
import sys, time, argparse, copy
import itertools
import subprocess
import os, os.path
import array
from math import sqrt, log, isnan, isinf

from prot import filetools
from prot import canvastools
from prot import utiltools
from prot import style
from prot import batchtools
import dijettools

import ROOT
import DijetFuncs
import Minuit2Fit
import singleFit

import multiprocessing
import plotSingleFit

def prepfit(dataHist,fitToUse,massRanges=[],f_chi2=False):
    ### Determine mass ranges to fit ###
    if len(massRanges)%2 is not 0:
        print("Error, massRanges needs an even number of arguments")
        return

    minMjj = []
    maxMjj = []
    for iRange in range(int(len(massRanges)/2)):
        minMjj.append( massRanges[2*iRange] )
        maxMjj.append( massRanges[2*iRange+1] )

    ## Get corresponding bins to use ##
    minMjj,maxMjj,binList=singleFit.rangeMjjBin(dataHist,minMjj,maxMjj)

    ## Prepare histogram with fitted bins ##
    binHist=singleFit.prepareBinHist(dataHist, binList)

    ## Setup our Fitting Function ##
    fitInfo     = DijetFuncs.getFit( fitToUse )
    thisFitter  = singleFit.setupFitter(fitInfo,f_chi2=f_chi2)
    thisFunctor = Minuit2Fit.groupFNC(fitInfo,dataHist,binList)
    thisFunctor.f_chi2=f_chi2

    return thisFitter,thisFunctor

def runfit(dataHist,fitToUse,massRanges=[],f_chi2=False,aParams=None):
    thisFitter,thisFunctor=prepfit(dataHist,fitToUse,massRanges,f_chi2)

    #### Fit to data hist and get best fit parameters ####
    aParams=thisFunctor.fitInfo.getFuncionParametersArray() if aParams==None else aParams
    if f_chi2:
        Minuit2Fit.FitWithRetries(5, thisFitter, thisFunctor, aParams)
    else:
        Minuit2Fit.Chi2LogFit(thisFitter,thisFunctor, aParams)
    ## Save final values ##
    fitResults=[]
    fitResults.append(singleFit.calcChi2(thisFitter,thisFunctor))
    fitResults.append(singleFit.calcNLL(thisFitter,thisFunctor))
    chi2=fitResults[0].Chi2()
    ndof=fitResults[0].Ndf()
    print('CHI2\t%f\t%f\t%f'%(chi2,ndof,ROOT.TMath.Prob(chi2,ndof)))
    return fitResults

def runSingleFit(infile,histpath,fitsToUse=['Dijet4'],massRanges=[200,500],bootstrap=False,f_chi2=False,outpath=None):
    ### Get input histogram ###
    # dataHist1 = utiltools.Get('%s:/%s'%(infile,histpath.replace('DataLike','Scaled')))
    # dataHist=dataHist1.Clone()
    # dataHist.Reset()
    # dataHist.FillRandom(dataHist1,ROOT.TRandom3().Poisson(int(dataHist1.Integral())))

    dataHist = utiltools.Get('%s:/%s'%(infile,histpath))

    ### Get informations about output ###
    parts=histpath.split('/')
    selection=parts[0]
    histname=parts[-1]
    lumi=dijettools.lumiint(histname.split('_')[-1])

    d_out=None
    fh_out=None
    if outpath!=None:
        fh_out=filetools.open(outpath,'RECREATE')
        d_out=filetools.mkdir(fh_out,selection)

    ### FIT! ###
    fitResults=None
    for fitToUse in fitsToUse:
        fitInfo = DijetFuncs.getFit( fitToUse )

        # Get initial parameters
        aParams=None
        if bootstrap:
            aParams=[]
            for pIdx in range(len(fitInfo.fParams)):
                if fitResults==None or pIdx>=fitResults[-1].NPar():
                    aParams.append(fitInfo.fParams[pIdx])
                else:
                    aParams.append(fitResults[-1].Parameter(pIdx))
            aParams=array.array('d', aParams)

        # Run fit
        fitResults=runfit(dataHist,fitToUse,massRanges=massRanges,f_chi2=f_chi2,aParams=aParams)

        # Save the fit results
        if outpath!=None:
            d_out.cd()
            fitResultAppend='_%s_%s'%(fitToUse,histname)
            #binHist.Write(binHist.GetName()+fitResultAppend, ROOT.TObject.kWriteDelete)
            for fitResult in fitResults:
                fitResult.Write(fitResult.GetName()+fitResultAppend, ROOT.TObject.kWriteDelete)

        # Print the fit results
        plotSingleFit.plotfits(dataHist,[(fitToUse,fitResults[-1])],xrange=(min(massRanges),max(massRanges)),logy=True,selection=selection,lumi=lumi)

def main(infile,massRanges=[200,1500],fitsToUse=['Dijet3','Dijet4','Dijet5','Dijet6'],bootstrap=False,f_chi2=False,outFile='fit.root'):
    ### Get input data file ###
    fileIn = filetools.open(infile,'UPDATE')
    if not fileIn:
        print("ERROR, could not find file ", infile)
        exit(1)

    # Process selections
    job=batchtools.get_batch('runSingleFit')
    job.data.add(infile)
    job.output.add('output.root')

    dijetdata=dijettools.DijetData(fileIn)
    for selection,lumidatas in dijetdata.selections.items():
        for lumi,item in lumidatas.items():
            if selection not in ['trijet_j430_2j25','dijetgamma_g150_2j25']: continue
            job.commands.append((runSingleFit,[infile,
                                               selection+'/'+item.h_datalike.GetName()],
                                               {'fitsToUse' :fitsToUse,
                                                'massRanges':massRanges,
                                                'bootstrap' :bootstrap,
                                                'f_chi2'    :f_chi2,
                                                'outpath'   :'output.root'}))

    fileIn.Close()
    tempdir=job.run()

    #
    # Merge
    if os.path.exists(outFile):
        os.unlink(outFile)

    fh_out=filetools.open('output.root','RECREATE')
    fh_out.Close()

    subprocess.check_call(['hadd',outFile,tempdir+'/output.root',infile])

if __name__ == "__main__":
  main()
  print("Finished main()")
