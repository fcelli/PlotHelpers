#!/bin/bash

if [ ${#} != 1 ]; then
    echo "usage: ${0} sample"
    exit -1
fi

SAMPLE=${1}

if [ ! -d ${SAMPLE} ]; then
    mkdir ${SAMPLE}
fi

DATADIR=/global/projecta/projectdirs/atlas/${USER}/batch

sshfs -o ServerAliveInterval=15 -o reconnect cori:${DATADIR}/${SAMPLE} ${SAMPLE}
